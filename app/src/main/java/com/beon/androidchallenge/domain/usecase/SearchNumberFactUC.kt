package com.beon.androidchallenge.domain.usecase

import com.beon.androidchallenge.data.repository.FactRepository
import com.beon.androidchallenge.ui.main.state.MainState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import javax.inject.Inject

class SearchNumberFactUC @Inject constructor(
    private val factRepository: FactRepository
) {
    suspend fun search(number: String): MainState = withContext(Dispatchers.IO) {
        if (number.isEmpty()) MainState.Empty else getFact(number)
    }

    private suspend fun getFact(number: String): MainState {
        val fact = factRepository.getFactForNumber(number = number)
        return if (fact.text.isNullOrBlank()) {
            MainState.Error
        } else {
            MainState.FactFound(fact)
        }
    }
}
