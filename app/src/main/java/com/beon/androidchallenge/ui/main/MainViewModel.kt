package com.beon.androidchallenge.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.beon.androidchallenge.domain.usecase.SearchNumberFactUC
import com.beon.androidchallenge.ui.main.state.MainState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val searchNumberFactUC: SearchNumberFactUC
) : ViewModel() {

    val currentFact = MutableLiveData<MainState>(MainState.Empty)

    private val debounce = MutableSharedFlow<String>()

    init {
        viewModelScope.launch(
            CoroutineExceptionHandler { _, _ -> postError() }
        ) {
            debounce.debounce(ONE_SECOND_IN_MILLIS)
                .flowOn(Dispatchers.Default)
                .collect {number ->
                    currentFact.postValue(MainState.Loading)
                    val state = searchNumberFactUC.search(number)
                    currentFact.postValue(state)
                }
        }
    }

    fun searchNumberFact(number: String) = viewModelScope.launch(
        CoroutineExceptionHandler { _, _ -> postError() }
    ) {
        debounce.emit(number)
    }

    private fun postError() = currentFact.postValue(MainState.Error)

    companion object {
        private const val ONE_SECOND_IN_MILLIS = 1000L
    }
}
