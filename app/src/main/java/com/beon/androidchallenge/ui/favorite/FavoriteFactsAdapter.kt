package com.beon.androidchallenge.ui.favorite

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.beon.androidchallenge.databinding.FavoriteItemBinding
import com.beon.androidchallenge.domain.model.Fact

class FavoriteFactsAdapter : RecyclerView.Adapter<FavoriteFactsAdapter.FactViewHolder>() {

    private val facts: List<Fact> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FactViewHolder {
        val binding = FavoriteItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return FactViewHolder(binding)
    }

    override fun getItemCount(): Int {
        TODO("Not yet implemented")
    }

    override fun onBindViewHolder(holder: FactViewHolder, position: Int) {
        TODO("Not yet implemented")
    }

    class FactViewHolder(binding: FavoriteItemBinding) : ViewHolder(binding.root) {

    }
}
