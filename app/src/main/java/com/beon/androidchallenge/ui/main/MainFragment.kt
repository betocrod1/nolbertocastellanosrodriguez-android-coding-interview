package com.beon.androidchallenge.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.beon.androidchallenge.R
import com.beon.androidchallenge.databinding.MainFragmentBinding
import com.beon.androidchallenge.domain.model.Fact
import com.beon.androidchallenge.ui.main.state.MainState
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }


    private lateinit var viewModel: MainViewModel

    private var _binding: MainFragmentBinding? = null
    private val binding: MainFragmentBinding
        get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = MainFragmentBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        initViews()
        initObservers()
    }

    private fun initViews() {
        binding.run {
            setupNumberEditText()
            setupRetryButton()
            requestFocusForNumberEditText()
        }
    }

    private fun MainFragmentBinding.requestFocusForNumberEditText() =
        numberEditText.requestFocus()

    private fun MainFragmentBinding.setupRetryButton() {
        retryButton.setOnClickListener {
            viewModel.searchNumberFact(numberEditText.text.toString())
        }
    }

    private fun MainFragmentBinding.setupNumberEditText() {
        numberEditText.addTextChangedListener {
            viewModel.searchNumberFact(it.toString())
        }

        numberEditText.setOnEditorActionListener { textView, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                viewModel.searchNumberFact(textView.text.toString())
                return@setOnEditorActionListener true
            } else {
                return@setOnEditorActionListener false
            }
        }
    }

    private fun initObservers() {
        viewModel.currentFact.observe(viewLifecycleOwner) { state ->
            when (state) {
                MainState.Empty -> showEmptyMessage()
                MainState.Error -> showErrorMessage()
                is MainState.FactFound -> showFactMessage(state.fact)
                MainState.Loading -> showLoader()
            }
        }
    }

    private fun showLoader() {
        binding.progressBar.visibility = View.VISIBLE
        binding.factTextView.visibility = View.INVISIBLE
    }

    private fun hideLoader() {
        binding.progressBar.visibility = View.GONE
    }

    private fun showFactMessage(fact: Fact) {
        binding.factTextView.text = fact.text
        binding.factTextView.visibility = View.VISIBLE
        hideLoader()
    }

    private fun showErrorMessage() {
        binding.factTextView.setText(R.string.error)
        binding.factTextView.visibility = View.VISIBLE
        hideLoader()
    }

    private fun showEmptyMessage() {
        binding.factTextView.setText(R.string.instructions)
        hideLoader()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
