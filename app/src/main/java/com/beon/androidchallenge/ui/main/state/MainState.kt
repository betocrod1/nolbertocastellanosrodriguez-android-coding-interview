package com.beon.androidchallenge.ui.main.state

import com.beon.androidchallenge.domain.model.Fact

sealed class MainState {
    object Empty : MainState()
    object Error : MainState()
    class FactFound(val fact: Fact) : MainState()
    object Loading: MainState()
}
