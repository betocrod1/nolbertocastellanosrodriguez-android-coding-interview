package com.beon.androidchallenge.data.repository

import android.util.LruCache
import com.beon.androidchallenge.domain.model.Fact
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FactLocalDataSource @Inject constructor() {

    private val cache = object : LruCache<String, Fact>(MAX_FACT_CACHE_SIZE_BYTES) {

        override fun sizeOf(key: String?, value: Fact?): Int {
            return (value?.text?.length ?: 0) * BYTE_IN_BITS
        }
    }

    fun getFactForNumber(number: String): Fact? {
        return cache[number]
    }

    fun saveFactForNumber(number: String, fact: Fact) {
        cache.put(number, fact)
    }

    companion object {
        private const val MAX_FACT_CACHE_SIZE_BYTES = 100
        private const val BYTE_IN_BITS = 8
    }
}
