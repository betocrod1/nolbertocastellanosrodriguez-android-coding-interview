package com.beon.androidchallenge.data.network

import com.beon.androidchallenge.domain.model.Fact
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject

class Api @Inject constructor() {

    private val client: FactsClient

    init {
        val httpClient = OkHttpClient.Builder()

        val builder: Retrofit.Builder = Retrofit.Builder()
            .baseUrl(API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
        val retrofit: Retrofit = builder.client(httpClient.build()).build()
        client = retrofit.create(FactsClient::class.java)
    }

    suspend fun getFactForNumber(number: String): Fact {
        return client.getFactForNumber(number)
    }

    companion object {
        private const val API_BASE_URL = "http://numbersapi.com/"
    }
}
