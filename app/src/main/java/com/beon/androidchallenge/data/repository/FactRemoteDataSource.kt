package com.beon.androidchallenge.data.repository

import com.beon.androidchallenge.data.network.Api
import com.beon.androidchallenge.domain.model.Fact
import javax.inject.Inject

class FactRemoteDataSource @Inject constructor(
    private val api: Api
) {
    suspend fun getFactForNumber(number: String): Fact {
        return api.getFactForNumber(number)
    }
}
