package com.beon.androidchallenge.data.repository

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class FactRepository @Inject constructor(
    private var factRemoteDataSource: FactRemoteDataSource,
    private var factLocalDataSource: FactLocalDataSource
) {
    suspend fun getFactForNumber(number: String) =
        withContext(Dispatchers.IO) {
            factLocalDataSource.getFactForNumber(number) ?: run {
                val fact = factRemoteDataSource.getFactForNumber(number)
                factLocalDataSource.saveFactForNumber(number, fact)
                fact
            }
        }
}
